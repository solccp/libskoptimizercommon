#ifndef INPUT_H
#define INPUT_H

#include <Variant/Variant.h>

#include <memory>
#include <stdexcept>
#include <QString>
#include <QDir>

#include "skbuilder/input.h"

#include "DFTBTestSuite/InputFile.h"
#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"

#include "erepfit2_inputdata.h"


namespace DFTBPSO
{
    namespace Input
    {

        struct GuessVector
        {
            QString File;
            int startingIteration = 1;
            double Spreading = 0.03;
        };

        struct Guess
        {
            double Occupancy = 0.1;
            QList<GuessVector> items;
        };


        template<class T>
        class KeywordWithExternalFile
        {
        public:
            QString input_file;
            std::shared_ptr<T> data;
        };

        class Tester
        {
        public:
            DFTBTestSuite::InputData input;
            DFTBTestSuite::ReferenceData reference;
        public:
            QString input_file;
            QString reference_file;
        };


        struct AdditionalFitness
        {
            QString type = "script";
            QString source;
            double weight = 1.0;
        };

        struct PSO
        {
            int numOfParticles = 1;
            int numOfIterations = 1;

            QString algorithm = "pso";
            int MOPSOMaxRepoSize = 20;
            Guess guess;
            AdditionalFitness additionalFitness;

        };

        class ErepfitAdditionalEquation
        {
        public:
            QPair<QString, QString> potentialName;
            int derivative;
            double distance;
            QPair<double, double> range;
        };

        class ExternalRepulsivePotentials
        {
        public:
             QMap<QString, QString> m_data;
        };


        class RepulsivePotentials
        {
        public:
            QPair<double, double> m_PotentialRanges;
            int m_PotentialKnots;
            bool m_aggressive_initialization = false;
        };


        class Options
        {
        public:
            QString scratchDir = QDir::tempPath();
            unsigned int randomSeed = 0;
            bool debug = false;
        };



        class ConfiningParamaterRange
        {
        public:
            virtual ~ConfiningParamaterRange(){}
            virtual libvariant::Variant toVariant() const = 0;
            virtual void fromVariant(const libvariant::Variant& var) = 0;
        };

        class ConfiningParamaterRange_NCTU : public ConfiningParamaterRange
        {
        public:
            QPair<double, double> r;
            QPair<double, double> a;
            QPair<double, double> w;
            virtual libvariant::Variant toVariant() const
            {
                libvariant::Variant var;
                var["w"] = libvariant::VariantConvertor<QPair<double, double> >::toVariant(w);
                var["r"] = libvariant::VariantConvertor<QPair<double, double> >::toVariant(r);
                var["a"] = libvariant::VariantConvertor<QPair<double, double> >::toVariant(a);
                return var;
            }
            virtual void fromVariant(const libvariant::Variant &var)
            {
                using pdd = QPair<double, double>;
                GET_VARIANT(r, var, "r", pdd);
                GET_VARIANT(a, var, "a", pdd);
                GET_VARIANT(w, var, "w", pdd);
            }
        };

        class ConfiningParamaterRange_BCCMS : public ConfiningParamaterRange
        {
        public:
            QPair<double, double> r;
            QPair<double, double> n;
            virtual libvariant::Variant toVariant() const
            {
                libvariant::Variant var;
                var["r"] = libvariant::VariantConvertor<QPair<double, double> >::toVariant(r);
                var["n"] = libvariant::VariantConvertor<QPair<double, double> >::toVariant(n);
                return var;
            }
            virtual void fromVariant(const libvariant::Variant &var)
            {
                using pdd = QPair<double, double>;
                GET_VARIANT(r, var, "r", pdd);
                GET_VARIANT(n, var, "n", pdd);
            }
        };


        class Confining
        {
        public:
            QStringList orbitalTypes;
            std::shared_ptr<ConfiningParamaterRange> ranges;
        };


        class ConfiningAction
        {
        public:
            QStringList take;
            QList<Confining> confinings;
        };


        class OrbitalEnergyDefinition
        {
        public:
            QString orbital;
            QPair<double, double> range;
            QString referredOrbital;
        };

        struct HubbardDefinition
        {
            QString orbital;
            QPair<double, double> range;
        };

        class AtomicDefinition
        {
        public:
            QString element;
            QList<ConfiningAction> confining_actions;
            QList<OrbitalEnergyDefinition> orbitalEnergyDefinitions;
            QList<HubbardDefinition> hubbardDefinitions;
            QList<HubbardDefinition> hubbardDerivativeDefinitions;
        };


        class ElectronicPart
        {
        public:
            QList<AtomicDefinition> atomicDefinitions;
        };


        struct ErepfitPSOVector
        {
//            bool useExperimentMethod = true;
            Erepfit2::Input::Erepfit2_InputData erepfit2_input_data;
            QString erepfit2_input_file;
            QMap<QString, RepulsivePotentials> repulsivePotentials;
            QList<ErepfitAdditionalEquation> additionalEquations;
        };

        struct RepulsivePart
        {
            QString repulsiveType = "erepfit";
            bool fourthOrder = true;
            std::shared_ptr<ErepfitPSOVector> erepfitPSOVector;
        };

        struct LJDispersion
        {
            QString element;
            QPair<double, double> distance_range;
            QPair<double, double> energy_range;
        };

        struct D3Dispersion
        {
            QString damping;
            QPair<double, double> s6;
            QPair<double, double> s8;
            QPair<double, double> a1;
            QPair<double, double> a2;
            QPair<double, double> sr6;
            QPair<double, double> alpha6;
        };

        struct IndependentPart
        {
            QList<LJDispersion> ljDispersion;
            std::shared_ptr<D3Dispersion> d3Dispersion;
            bool optDampingFactor = false;
            QPair<double, double> dampingFactorRange;
        };

        class PSOVector
        {
        public:
            std::shared_ptr<ElectronicPart> electronicPart;
            std::shared_ptr<RepulsivePart> repulsivePart;
            std::shared_ptr<IndependentPart> independendPart;
        };


        class SKOPTInput
        {
        public:
            Options options;
            std::shared_ptr<ADPT::SKFileInfo> external_elec_skfileInfo;

            PSO pso;
            Tester tester;
            PSOVector psoVector;

            QString skbuilder_input_file;
            std::shared_ptr<SKBuilder::Input::SKBuilderInput> skbuilder_input;

          public:
            QMap<QString, QString> external_rep_skfiles;
            QMap<QString, QStringList> parsed_external_elec_skpairs;
        };

    }
}
#endif // INPUT_H
