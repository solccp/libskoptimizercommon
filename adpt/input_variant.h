#ifndef INPUT_JSON_H
#define INPUT_JSON_H


#include <cstdlib>     /* srand, rand */
#include <ctime>       /* time */

#include "input.h"


#include <variantqt.h>
#include <Variant/Variant.h>

#include "DFTBTestSuite/input_variant.h"
#include "DFTBTestSuite/ReferenceDataType/referencedata_variant.h"
#include "skbuilder/input_variant.h"

#include "erepfit2_inputdata_json.h"


namespace libvariant
{
    template<>
    struct VariantConvertor<DFTBPSO::Input::GuessVector>
    {
        static Variant toVariant(const DFTBPSO::Input::GuessVector &rhs)
        {
            Variant var;
            var["file"] = rhs.File.toStdString();
            var["spreading"] = rhs.Spreading;
            var["starting_iteration"] = rhs.startingIteration;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::GuessVector &rhs)
        {
            try
            {
                bool hasValue = false;
                GET_VARIANT_OPT(rhs.File, var, "file", QString, hasValue);
                GET_VARIANT_OPT(rhs.Spreading, var, "spreading", double, hasValue);
                GET_VARIANT_OPT(rhs.startingIteration, var, "starting_iteration", int, hasValue);
                Q_UNUSED(hasValue);
            }
            catch (const std::exception& e)
            {
                std::cout << e.what() << std::endl;
                throw e;
            }
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::Options>
    {
        static Variant toVariant(const DFTBPSO::Input::Options &rhs)
        {
            Variant var;
            var["debug"] = rhs.debug;
            if (rhs.randomSeed > 0)
            {
                var["random_seed"] = rhs.randomSeed;
            }
            var["scratch_dir"] = rhs.scratchDir.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::Options &rhs)
        {
            try
            {
                bool hasValue = false;
                GET_VARIANT_OPT(rhs.debug, var, "debug", bool, hasValue);
                GET_VARIANT_OPT(rhs.randomSeed, var, "random_seed", unsigned int, hasValue);
                if (!hasValue || (hasValue && (rhs.randomSeed <= 0)))
                {
                    std::srand (std::time(NULL));
                    rhs.randomSeed = std::rand()%10000;
                }

                GET_VARIANT_OPT(rhs.scratchDir, var, "scratch_dir", QString, hasValue);
                Q_UNUSED(hasValue);
            }
            catch (const std::exception& e)
            {
                std::cout << e.what() << std::endl;
                throw e;
            }
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::Tester>
    {
        static Variant toVariant(const DFTBPSO::Input::Tester &rhs)
        {
            Variant var;
            if (!rhs.reference_file.isEmpty())
            {
                var["reference_file"] = rhs.reference_file.toStdString();
            }
            else
            {
                var["reference_data"] = VariantConvertor<DFTBTestSuite::ReferenceData>::toVariant(rhs.reference);
            }

            if (!rhs.reference_file.isEmpty())
            {
                var["input_file"] = rhs.input_file.toStdString();
            }
            else
            {
                var["input_data"] =  VariantConvertor<DFTBTestSuite::InputData>::toVariant(rhs.input);
            }

            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::Tester &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.reference_file, var, "reference_file", QString, hasValue);
            GET_VARIANT_OPT(rhs.input_file, var, "input_file", QString, hasValue);
            GET_VARIANT_OPT(rhs.reference, var, "reference_data", DFTBTestSuite::ReferenceData, hasValue);
            GET_VARIANT_OPT(rhs.input, var, "input_data", DFTBTestSuite::InputData, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::Guess>
    {
        static Variant toVariant(const DFTBPSO::Input::Guess &rhs)
        {
            Variant var;
            var["vectors"] = VariantConvertor<QList<DFTBPSO::Input::GuessVector> >::toVariant(rhs.items);
            var["occupation"] = rhs.Occupancy;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::Guess &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.items, var, "vectors", QList<DFTBPSO::Input::GuessVector>, hasValue);
            GET_VARIANT_OPT(rhs.Occupancy, var, "occupation", double, hasValue);
            Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBPSO::Input::AdditionalFitness>
    {
        static Variant toVariant(const DFTBPSO::Input::AdditionalFitness &rhs)
        {
            Variant var;
            var["type"] = rhs.type.toStdString();
            var["source"] = rhs.source.toStdString();
            var["weight"] = rhs.weight;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::AdditionalFitness &rhs)
        {
            bool hasValue;
            Q_UNUSED(hasValue)
            GET_VARIANT_OPT(rhs.type, var, "type", QString, hasValue);
            GET_VARIANT_OPT(rhs.source, var, "source", QString, hasValue);
            GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);
        }
    };



    template<>
    struct VariantConvertor<DFTBPSO::Input::PSO>
    {
        static Variant toVariant(const DFTBPSO::Input::PSO &rhs)
        {
            Variant var;
            var["guess"] = VariantConvertor<DFTBPSO::Input::Guess>::toVariant(rhs.guess);
            var["num_of_iterations"] = rhs.numOfIterations;
            var["num_of_particles"] = rhs.numOfParticles;
            var["algorithm"] = rhs.algorithm.toStdString();
            var["additional_fitness"] = VariantConvertor<DFTBPSO::Input::AdditionalFitness>::toVariant(rhs.additionalFitness);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::PSO &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.guess, var, "guess", DFTBPSO::Input::Guess, hasValue);
            GET_VARIANT_OPT(rhs.numOfIterations, var, "num_of_iterations", int, hasValue);
            GET_VARIANT_OPT(rhs.numOfParticles, var, "num_of_particles", int, hasValue);
            GET_VARIANT_OPT(rhs.algorithm, var, "algorithm", QString, hasValue);         
            GET_VARIANT_OPT(rhs.additionalFitness, var, "additional_fitness", DFTBPSO::Input::AdditionalFitness, hasValue);
            Q_UNUSED(hasValue);
        }
    };
    template<>
    struct VariantConvertor<DFTBPSO::Input::RepulsivePotentials>
    {
        static Variant toVariant(const DFTBPSO::Input::RepulsivePotentials &rhs)
        {
            Variant var;
            var["aggressive_initialization"] = rhs.m_aggressive_initialization;
            var["range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.m_PotentialRanges);
            var["num_knots"] = rhs.m_PotentialKnots;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::RepulsivePotentials &rhs)
        {
                bool hasValue;
                GET_VARIANT_OPT(rhs.m_aggressive_initialization, var, "aggressive_initialization", bool, hasValue);
                typedef QPair<double, double> pdd;
                GET_VARIANT_OPT(rhs.m_PotentialRanges, var, "range", pdd, hasValue);
                GET_VARIANT_OPT(rhs.m_PotentialKnots, var, "num_knots", int, hasValue);
                Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBPSO::Input::ErepfitAdditionalEquation>
    {
        static Variant toVariant(const DFTBPSO::Input::ErepfitAdditionalEquation &rhs)
        {
            Variant var;
            var["derivative"] = rhs.derivative;
            var["distance"] = rhs.distance;
            var["range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.range);
            var["potential"] = QString("%1-%2").arg(rhs.potentialName.first).arg(rhs.potentialName.second).toStdString();

            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::ErepfitAdditionalEquation &rhs)
        {
            bool hasValue;
            typedef QPair<double, double> pdd;
            GET_VARIANT(rhs.derivative, var, "derivative", int);
            GET_VARIANT(rhs.distance, var, "distance", double);
            GET_VARIANT(rhs.range, var, "range", pdd);
            QString poten;
            GET_VARIANT(poten, var, "potential", QString);
            QStringList arr = poten.split("-", QString::SkipEmptyParts);
            rhs.potentialName.first = arr[0];
            rhs.potentialName.second = arr[1];
            Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBPSO::Input::ErepfitPSOVector>
    {
        static Variant toVariant(const DFTBPSO::Input::ErepfitPSOVector &rhs)
        {
            Variant var;
            var["potential_grids"] = VariantConvertor<QMap<QString, DFTBPSO::Input::RepulsivePotentials> >::toVariant(rhs.repulsivePotentials);
            var["erepfit_input_data"] = VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::toVariant(rhs.erepfit2_input_data);
            var["additional_equations"] = VariantConvertor<QList<DFTBPSO::Input::ErepfitAdditionalEquation> >::toVariant(rhs.additionalEquations);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::ErepfitPSOVector &rhs)
        {
                bool hasValue;
                GET_VARIANT_OPT(rhs.erepfit2_input_file, var, "erepfit_input_file", QString, hasValue);
                GET_VARIANT_OPT(rhs.erepfit2_input_data, var, "erepfit_input_data", Erepfit2::Input::Erepfit2_InputData, hasValue);

                typedef QMap<QString, DFTBPSO::Input::RepulsivePotentials> mapp;
                GET_VARIANT_OPT(rhs.repulsivePotentials, var, "potential_grids", mapp, hasValue);
                GET_VARIANT_OPT(rhs.additionalEquations, var, "additional_equations", QList<DFTBPSO::Input::ErepfitAdditionalEquation>, hasValue);

                ///todo: external repulsive potential.

                Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBPSO::Input::RepulsivePart>
    {
        static Variant toVariant(const DFTBPSO::Input::RepulsivePart &rhs)
        {
            Variant var;
            var["erepfit_psovector"] = VariantConvertor<DFTBPSO::Input::ErepfitPSOVector>::toVariant(*rhs.erepfitPSOVector);
            var["fourth_order_spline"] = rhs.fourthOrder;
            var["repulsive_type"] = rhs.repulsiveType.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::RepulsivePart &rhs)
        {
            bool hasValue;
            GET_VARIANT(rhs.repulsiveType, var, "repulsive_type", QString);
            GET_VARIANT_OPT(rhs.fourthOrder, var, "fourth_order_spline", bool, hasValue);
            if (rhs.repulsiveType.toLower() == "erepfit")
            {
                GET_VARIANT_PTR(rhs.erepfitPSOVector, var, "erepfit_psovector", DFTBPSO::Input::ErepfitPSOVector);
            }

            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::HubbardDefinition>
    {
        static Variant toVariant(const DFTBPSO::Input::HubbardDefinition &rhs)
        {
            Variant var;
            var["orbital"] = rhs.orbital.toStdString();
            var["range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.range);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::HubbardDefinition &rhs)
        {
            GET_VARIANT(rhs.orbital, var, "orbital", QString);
            typedef QPair<double, double> pdd;
            GET_VARIANT(rhs.range, var, "range", pdd);
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::OrbitalEnergyDefinition>
    {
        static Variant toVariant(const DFTBPSO::Input::OrbitalEnergyDefinition &rhs)
        {
            Variant var;
            var["orbital"] = rhs.orbital.toStdString();
            var["range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.range);
            var["referred_orbital"] = rhs.referredOrbital.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::OrbitalEnergyDefinition &rhs)
        {
            GET_VARIANT(rhs.orbital, var, "orbital", QString);
            typedef QPair<double, double> pdd;
            GET_VARIANT(rhs.range, var, "range", pdd);
            GET_VARIANT(rhs.referredOrbital, var, "referred_orbital", QString);
        }
    };


    template<>
    struct VariantConvertor<DFTBPSO::Input::ConfiningAction>
    {
        static Variant toVariant(const DFTBPSO::Input::ConfiningAction &rhs)
        {
            Variant var;
            var["take"] = VariantConvertor<QStringList>::toVariant(rhs.take);
            var["confining"] = VariantConvertor<QList<DFTBPSO::Input::Confining> >::toVariant(rhs.confinings);
            return var;
        }
        static void fromVariant_1(const Variant &var, DFTBPSO::Input::ConfiningAction &rhs, const QString& toolkit_name)
        {
            bool hasValue;
            GET_VARIANT(rhs.take, var, "take", QStringList);
            using conf_lsit = QList<DFTBPSO::Input::Confining>;
            GET_VARIANT_1(rhs.confinings, var, "confining", conf_lsit, toolkit_name);
            Q_UNUSED(hasValue);
        }
    };




    template<>
    struct VariantConvertor<DFTBPSO::Input::Confining>
    {
        static Variant toVariant(const DFTBPSO::Input::Confining &rhs)
        {
            Variant var;
            if (rhs.ranges)
            {
                var["ranges"] = rhs.ranges->toVariant();
            }
            var["orbital_types"] = VariantConvertor<QStringList>::toVariant(rhs.orbitalTypes);
            return var;
        }
        static void fromVariant_1(const Variant &var, DFTBPSO::Input::Confining &rhs, const QString& toolkit_name)
        {

            bool hasValue;
            GET_VARIANT(rhs.orbitalTypes, var, "orbital_types", QStringList);

            if (var.Contains("ranges"))
            {
                if (toolkit_name.toLower() == "nctu")
                {
                    rhs.ranges =
                            std::static_pointer_cast<DFTBPSO::Input::ConfiningParamaterRange>
                            (std::make_shared<DFTBPSO::Input::ConfiningParamaterRange_NCTU>());
                }
                else if (toolkit_name.toLower() == "bccms")
                {
                    rhs.ranges = std::static_pointer_cast<DFTBPSO::Input::ConfiningParamaterRange>
                            (std::make_shared<DFTBPSO::Input::ConfiningParamaterRange_BCCMS>());
                }
                try
                {
                    if (rhs.ranges)
                    {
                        rhs.ranges->fromVariant(var["ranges"]);
                    }
                }
                catch(...)
                {
                    throw KeywordParseError("ranges");
                }
            }
            else
            {
                throw KeywordNotFound("ranges");
            }
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::AtomicDefinition>
    {
        static Variant toVariant(const DFTBPSO::Input::AtomicDefinition &rhs)
        {
            Variant var;
            var["confining_actions"] = VariantConvertor<QList<DFTBPSO::Input::ConfiningAction> >::toVariant(rhs.confining_actions);
            var["hubbard"] = VariantConvertor<QList<DFTBPSO::Input::HubbardDefinition> >::toVariant(rhs.hubbardDefinitions);
            var["hubbard_derivative"] = VariantConvertor<QList<DFTBPSO::Input::HubbardDefinition> >::toVariant(rhs.hubbardDerivativeDefinitions);
            var["orbital_energy"] = VariantConvertor<QList<DFTBPSO::Input::OrbitalEnergyDefinition> >::toVariant(rhs.orbitalEnergyDefinitions);
            return var;
        }
        static void fromVariant_1(const Variant &var, DFTBPSO::Input::AtomicDefinition &rhs, const QString& toolkit_name)
        {
            bool hasValue;
            GET_VARIANT_1_OPT(rhs.confining_actions, var, "confining_actions", QList<DFTBPSO::Input::ConfiningAction>, hasValue, toolkit_name);
            GET_VARIANT_OPT(rhs.hubbardDefinitions, var, "hubbard", QList<DFTBPSO::Input::HubbardDefinition>, hasValue);
            GET_VARIANT_OPT(rhs.hubbardDerivativeDefinitions, var, "hubbard_derivative", QList<DFTBPSO::Input::HubbardDefinition>, hasValue);
            GET_VARIANT_OPT(rhs.orbitalEnergyDefinitions, var, "orbital_energy", QList<DFTBPSO::Input::OrbitalEnergyDefinition>, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::ElectronicPart>
    {
        static Variant toVariant(const DFTBPSO::Input::ElectronicPart &rhs)
        {
            Variant var;
            for(int i=0; i<rhs.atomicDefinitions.size(); ++i)
            {
                Variant var_item = VariantConvertor<DFTBPSO::Input::AtomicDefinition>::toVariant(rhs.atomicDefinitions[i]);
                var["atomic_definitions"][rhs.atomicDefinitions[i].element.toStdString()]  = var_item;
            }

            return var;
        }
        static void fromVariant_1(const Variant &var, DFTBPSO::Input::ElectronicPart &rhs, const QString& toolkit_name )
        {
            bool hasValue;

            typedef QMap<QString, DFTBPSO::Input::AtomicDefinition> mapp;
            mapp temp;
            GET_VARIANT_1_OPT(temp, var, "atomic_definitions", mapp, hasValue, toolkit_name);
            if (hasValue)
            {
                QMapIterator<QString, DFTBPSO::Input::AtomicDefinition> it(temp);
                while(it.hasNext())
                {
                    it.next();
                    DFTBPSO::Input::AtomicDefinition atom_def = it.value();
                    atom_def.element = it.key();
                    rhs.atomicDefinitions.append(atom_def);
                }
            }

            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::LJDispersion>
    {
        static Variant toVariant(const DFTBPSO::Input::LJDispersion &rhs)
        {
            Variant var;
            var["distance_range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.distance_range);
            var["energy_range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.energy_range);
            var["element"] = VariantConvertor<QString>::toVariant(rhs.element);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::LJDispersion &rhs)
        {
            typedef QPair<double, double> pdd;
            GET_VARIANT(rhs.distance_range, var, "distance_range", pdd);
            GET_VARIANT(rhs.energy_range, var, "energy_range", pdd);
            GET_VARIANT(rhs.element, var, "element", QString);
        }
    };

    template<>
    struct VariantConvertor<DFTBPSO::Input::D3Dispersion>
    {
        static Variant toVariant(const DFTBPSO::Input::D3Dispersion &rhs)
        {

            using pdd = QPair<double, double>;
            Variant var;
            var["damping"] = libvariant::VariantConvertor<QString>::toVariant(rhs.damping);
            var["s6"] = libvariant::VariantConvertor<pdd>::toVariant(rhs.s6);
            var["s8"] = libvariant::VariantConvertor<pdd>::toVariant(rhs.s8);
            if (rhs.damping == "bj")
            {
                var["a1"] = libvariant::VariantConvertor<pdd>::toVariant(rhs.a1);
                var["a2"] = libvariant::VariantConvertor<pdd>::toVariant(rhs.a2);
            }
            else if (rhs.damping == "zero")
            {
                var["sr6"] = libvariant::VariantConvertor<pdd>::toVariant(rhs.sr6);
                var["alpha6"] = libvariant::VariantConvertor<pdd>::toVariant(rhs.alpha6);
            }

            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::D3Dispersion &rhs)
        {
            using pdd = QPair<double, double>;
            GET_VARIANT(rhs.damping, var, "damping", QString);
            GET_VARIANT(rhs.s6, var, "s6", pdd);
            GET_VARIANT(rhs.s8, var, "s8", pdd);
            if (rhs.damping == "bj")
            {
                GET_VARIANT(rhs.a1, var, "a1", pdd);
                GET_VARIANT(rhs.a2, var, "a2", pdd);
            }
            else if (rhs.damping == "zero")
            {
                GET_VARIANT(rhs.sr6, var, "sr6", pdd);
                GET_VARIANT(rhs.alpha6, var, "alpha6", pdd);
            }
        }
    };


    template<>
    struct VariantConvertor<DFTBPSO::Input::IndependentPart>
    {
        static Variant toVariant(const DFTBPSO::Input::IndependentPart &rhs)
        {
            Variant var;
            if (rhs.optDampingFactor)
            {
                Variant var_damping;
                var_damping["range"] = VariantConvertor<QPair<double, double> >::toVariant(rhs.dampingFactorRange);
                var["damping"] = var_damping;
            }
            if (!rhs.ljDispersion.isEmpty())
            {
                var["dispersion_lj"] = VariantConvertor<QList<DFTBPSO::Input::LJDispersion> >::toVariant(rhs.ljDispersion);
            }
            else if (rhs.d3Dispersion)
            {
                var["dispersion_d3"] = VariantConvertor<DFTBPSO::Input::D3Dispersion>::toVariant(*rhs.d3Dispersion);
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::IndependentPart &rhs)
        {
            typedef QPair<double, double> pdd;
            GET_VARIANT_OPT(rhs.dampingFactorRange, var, "damping", pdd, rhs.optDampingFactor);

            bool hasValue;
            GET_VARIANT_OPT(rhs.ljDispersion, var, "dispersion_lj", QList<DFTBPSO::Input::LJDispersion>, hasValue);
            GET_VARIANT_PTR_OPT(rhs.d3Dispersion, var, "dispersion_d3", DFTBPSO::Input::D3Dispersion, hasValue);
            Q_UNUSED(hasValue);
        }
    };



    template<>
    struct VariantConvertor<DFTBPSO::Input::PSOVector>
    {
        static Variant toVariant(const DFTBPSO::Input::PSOVector &rhs)
        {
            Variant var;

            if (rhs.repulsivePart)
            {
                var["repulsive_part"] = VariantConvertor<DFTBPSO::Input::RepulsivePart>::toVariant(*rhs.repulsivePart);
            }

            if (rhs.electronicPart)
            {
                var["electronic_part"] = VariantConvertor<DFTBPSO::Input::ElectronicPart>::toVariant(*rhs.electronicPart);
            }

            if (rhs.independendPart)
            {
                var["independent_part"] = VariantConvertor<DFTBPSO::Input::IndependentPart>::toVariant(*rhs.independendPart);
            }
            return var;
        }
//        static void fromVariant_1(const Variant &var, DFTBPSO::Input::PSOVector &rhs, const QString& toolkit_name)
//        {
//            try
//            {
//                bool hasValue;
//                GET_VARIANT_PTR_OPT(rhs.repulsivePart, var, "repulsive_part", DFTBPSO::Input::RepulsivePart, hasValue);
//                GET_VARIANT_PTR_1_OPT(rhs.electronicPart, var, "electronic_part", DFTBPSO::Input::ElectronicPart, hasValue, toolkit_name);
//                GET_VARIANT_PTR_OPT(rhs.independendPart, var, "independent_part", DFTBPSO::Input::IndependentPart, hasValue);
//                Q_UNUSED(hasValue);
//            }
//            catch (const std::exception& e)
//            {
//                std::cout << e.what() << std::endl;
//                throw e;
//            }
//        }
    };


    template<>
    struct VariantConvertor<DFTBPSO::Input::SKOPTInput>
    {
        static Variant toVariant(const DFTBPSO::Input::SKOPTInput &rhs)
        {
            Variant var;
            var["options"] =  VariantConvertor<DFTBPSO::Input::Options>::toVariant(rhs.options);
            var["pso"] =  VariantConvertor<DFTBPSO::Input::PSO>::toVariant(rhs.pso);

            var["tester"] = VariantConvertor<DFTBPSO::Input::Tester>::toVariant(rhs.tester);

            if (rhs.external_elec_skfileInfo)
            {
                var["external_electronic_skfileinfo"] = VariantConvertor<ADPT::SKFileInfo>::toVariant(*rhs.external_elec_skfileInfo);
            }           

            if (!rhs.external_rep_skfiles.empty())
            {
                var["external_repulsive_skfiles"] = VariantConvertor<QMap<QString, QString> >::toVariant(rhs.external_rep_skfiles);
            }


            if (!rhs.skbuilder_input_file.isEmpty())
            {
                var["skbuilder_input_file"] = rhs.skbuilder_input_file.toStdString();
            }
            else
            {
                if (rhs.skbuilder_input)
                {
                    var["skbuilder_input_data"] = VariantConvertor<SKBuilder::Input::SKBuilderInput>::toVariant(*rhs.skbuilder_input);
                }
            }

//            if (rhs.psoVector)
//            {
                var["pso_vector"] = VariantConvertor<DFTBPSO::Input::PSOVector>::toVariant(rhs.psoVector);
//            }

            return var;
        }
        static void fromVariant(const Variant &var, DFTBPSO::Input::SKOPTInput &rhs)
        {
            try
            {
                bool hasValue;
                using mss = QMap<QString, QString> ;
                GET_VARIANT(rhs.tester, var, "tester", DFTBPSO::Input::Tester);
                GET_VARIANT_OPT(rhs.options, var, "options", DFTBPSO::Input::Options, hasValue);
                GET_VARIANT_OPT(rhs.pso, var, "pso", DFTBPSO::Input::PSO, hasValue);
                GET_VARIANT_PTR_OPT(rhs.external_elec_skfileInfo, var, "external_electronic_skfileinfo", ADPT::SKFileInfo, hasValue);
                GET_VARIANT_OPT(rhs.external_rep_skfiles, var, "external_repulsive_skfiles", mss, hasValue);

                GET_VARIANT_OPT(rhs.skbuilder_input_file, var, "skbuilder_input_file", QString, hasValue);
                GET_VARIANT_PTR_OPT(rhs.skbuilder_input, var, "skbuilder_input_data", SKBuilder::Input::SKBuilderInput, hasValue);

                Q_UNUSED(hasValue);
            }
            catch(KeywordNotFound & e)
            {
                KeywordNotFound error(std::string("/") + e.what());
                throw error;
            }
            catch(KeywordParseError & e)
            {
                KeywordParseError error(std::string("/") + e.what());
                throw error;
            }
        }
    };
}
#endif // INPUT_JSON_H

