#-------------------------------------------------
#
# Project created by QtCreator 2013-10-02T22:14:33
#
#-------------------------------------------------

QT       -= gui
TARGET = SKOptimizerCommon
CONFIG += c++11
CONFIG += staticlib
TEMPLATE = lib
QMAKE_CXXFLAGS+=-Wunused-but-set-variable




INCLUDEPATH += $$PWD/../external_libs/include/
LIBS += $$PWD/../external_libs/libs/libVariant.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

HEADERS += \
    adpt/input.h \
    adpt/input_variant.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/release/ -lErepfitCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/debug/ -lErepfitCommon
else:unix: LIBS += -L$$OUT_PWD/../libErepfitCommon/ -lErepfitCommon

INCLUDEPATH += $$PWD/../libErepfitCommon
DEPENDPATH += $$PWD/../libErepfitCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/libErepfitCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/libErepfitCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/ErepfitCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/ErepfitCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/libErepfitCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/release/ -lSKBuilderCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/debug/ -lSKBuilderCommon
else:unix: LIBS += -L$$OUT_PWD/../libSKBuilderCommon/ -lSKBuilderCommon

INCLUDEPATH += $$PWD/../libSKBuilderCommon
DEPENDPATH += $$PWD/../libSKBuilderCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/libSKBuilderCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/libSKBuilderCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/SKBuilderCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/SKBuilderCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/libSKBuilderCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/release/ -lDFTBTestSuiteCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/debug/ -lDFTBTestSuiteCommon
else:unix: LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/ -lDFTBTestSuiteCommon

INCLUDEPATH += $$PWD/../libDFTBTestSuiteCommon
DEPENDPATH += $$PWD/../libDFTBTestSuiteCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/libDFTBTestSuiteCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/libDFTBTestSuiteCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/DFTBTestSuiteCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/DFTBTestSuiteCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/libDFTBTestSuiteCommon.a
